package news.tencent.charco.android.view.adapter;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import news.tencent.charco.android.R;
import news.tencent.charco.android.entity.NewsEs;
import news.tencent.charco.android.mimc.NewsEsSource;

/**
 * Created 18/7/21 16:34
 * Author:charcolee
 * Version:V1.0
 * ----------------------------------------------------
 * 文件描述：
 * ----------------------------------------------------
 */

public class SearchResultAdapter extends BaseQuickAdapter<NewsEs,BaseViewHolder> {

    public SearchResultAdapter(@Nullable List<NewsEs> data) {
        super(R.layout.item_search_result, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewsEs item) {
        if (null == item) {
            return;
        }

        NewsEsSource newsEsSource = item.getNewsEsSource();
        if (newsEsSource != null){
            TextView author = helper.getView(R.id.result_time);
            if (author != null){
                author.setText(newsEsSource.getTime());
            }
            TextView content = helper.getView(R.id.result_content);
            if (content != null){
                String txt = newsEsSource.getDigest() ;
                content.setText(txt);
            }
            TextView type = helper.getView(R.id.result_src);
            if (type != null){
                String txt = newsEsSource.getSrc() ;
                type.setText(txt);
            }
            TextView date = helper.getView(R.id.result_date);
            if (date != null && null != newsEsSource.getReleaseTime()){
                String txt = newsEsSource.getReleaseTime() ;
                date.setText(txt);
            }
            else {
                date.setText("");
            }
        }
    }
}
