package news.tencent.charco.android.mimc;

public interface MsgHandleInterface {
    /**
     * 单聊消息回调
     */
    public void handleMsg(MsgData news);

    /**
     * 群聊消息回调
     */
    public void handleUnlimitGroupMsg(MsgData news);
}
