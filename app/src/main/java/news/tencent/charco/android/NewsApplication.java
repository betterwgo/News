package news.tencent.charco.android;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.socks.library.KLog;

import java.util.UUID;

import news.tencent.charco.android.utils.ToastUtil;


/**
 * Created 18/5/18 14:34
 * Author:charcolee
 * Version:V1.0
 * ----------------------------------------------------
 * 文件描述：
 * ----------------------------------------------------
 */

public class NewsApplication extends Application {

    private static Context mContext;
    private static Thread mMainThread;//主线程
    private static long mMainThreadId;//主线程id
    private static Looper mMainLooper;//循环队列
    private static Handler mHandler;//主线程Handler

    @Override
    public void onCreate() {
        super.onCreate();
        if (mContext == null){
            mContext = getApplicationContext();
            init();
        }
    }

    private void init() {
        KLog.init(true,"ANews");//初始化KLog
        ToastUtil.init(getApplicationContext());
        mMainThread = Thread.currentThread();
        mMainThreadId = android.os.Process.myTid();
        mHandler = new Handler();
    }

    public static String getUserId(){
        String key = "userID";
        SharedPreferences preferences = getContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        String value = preferences.getString(key, "");
        if (null == value || "".equals(value)){
            UUID uuid = UUID.randomUUID();
            setValue(preferences,key, uuid.toString());
            value = uuid.toString();
        }
        return value;
    }

    //修改
    private static void setValue(SharedPreferences preferences,String key,String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static Context getContext(){
        return mContext;
    }

    public static Thread getMainThread() {
        return mMainThread;
    }


    public static long getMainThreadId() {
        return mMainThreadId;
    }


    public static Looper getMainThreadLooper() {
        return mMainLooper;
    }


    public static Handler getMainHandler() {
        return mHandler;
    }
}
