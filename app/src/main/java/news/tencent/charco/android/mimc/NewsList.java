package news.tencent.charco.android.mimc;

import java.util.List;

public class NewsList {
    public List<NewsEsSource> newsEsSourceList;

    public List<NewsEsSource> getNewsEsSourceList() {
        return newsEsSourceList;
    }

    public void setNewsEsSourceList(List<NewsEsSource> newsEsSourceList) {
        this.newsEsSourceList = newsEsSourceList;
    }
}
