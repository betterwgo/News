package news.tencent.charco.android.mimc;

public class Msg {
    /**
     * 新闻来源
     */
    private String src;
    /**
     * 新闻类型
     */
    private String newsType;
    /**
     * newsType下的再分类
     */
    private Integer subType;
    /**
     * 最近一条新闻的时间戳，
     * 用于查询哪些新闻是没有读的
     */
    private long latestTime;
    /**
     * newsType为detail情况下需要给出newsId
     */
    private String newsId;
    /**
     * 关键字查询
     */
    private String keywords;
    /**
     * 对应ES查询的from参数
     */
    private Integer pageFrom;
    /**
     * 对应ES查询的size参数
     */
    private Integer pageSize;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public long getLatestTime() {
        return latestTime;
    }

    public void setLatestTime(long latestTime) {
        this.latestTime = latestTime;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getPageFrom() {
        return pageFrom;
    }

    public void setPageFrom(Integer pageFrom) {
        this.pageFrom = pageFrom;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
