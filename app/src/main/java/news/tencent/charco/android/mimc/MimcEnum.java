package news.tencent.charco.android.mimc;

public enum MimcEnum {
    NEWS_KUAIXUN(1, "news_kuaixun"),
    NEWS_YW(2, "news_yw"),
    NEWS_ZHIBO(3, "news_zhibo"),
    NEWS_SSGS(4, "news_ssgs"),
    DYHJW_KX(5, "dyhjw_kuaixun"),
    DETAIL(6, "detail"),
    KEYWORD_SEARCH(7, "keyword_search");

    private Integer value;
    private String type;

    private MimcEnum(Integer value, String type) {
        this.value = value;
        this.type = type;
    }

    public Integer getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
