package news.tencent.charco.android.utils;

import android.util.Log;

import com.jiechic.library.android.snappy.Snappy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class CompressUtils {

    private static final String TAG = "CompressUtils";
    /**
     * 压缩文本
     *
     * @param text
     * @return
     */
    public static byte[] compressText(String text) {
        byte[] tempBytes = text.getBytes(StandardCharsets.UTF_8);
        Log.i(TAG,"压缩前：" + tempBytes.length);
        try {
            byte[] compressedText = Snappy.compress(tempBytes);
            Log.i(TAG,"压缩后：" + compressedText.length);
            return compressedText;
        } catch (IOException e) {
            Log.i(TAG,"compress text error, ", e);
        }
        return null;
    }

    /**
     * 解压文本
     *
     * @param bytes
     * @return
     */
    public static String deCompressText(byte[] bytes) {
        try {
            return Snappy.uncompressString(bytes);
        } catch (IOException e) {
            Log.i(TAG,"uncompress text error, ", e);
        }
        return null;
    }

}
