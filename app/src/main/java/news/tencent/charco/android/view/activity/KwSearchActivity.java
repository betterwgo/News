package news.tencent.charco.android.view.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import news.tencent.charco.android.R;
import news.tencent.charco.android.base.BaseActivity;
import news.tencent.charco.android.mimc.MsgData;
import news.tencent.charco.android.utils.UIUtils;
import news.tencent.charco.android.view.adapter.NewsDetailAdapter;

public class KwSearchActivity extends BaseActivity {

    private WebView mWebView;
    private RecyclerView mRecyclerView;
    private NewsDetailAdapter mAdapter;
    private AppBarLayout mAppBarLayout;
    private TextView mTvPublisher;
    private View mLltHead, mBarLayout;

    public static final String key = "KwSearchActivity.keywords";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        initView();
        ImageView imageView = findViewById(R.id.iv_back);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initView() {
        setWhiteStatus();
        mTvPublisher = findViewById(R.id.tv_publisher);
        mAppBarLayout = findViewById(R.id.app_bar);
        mLltHead = findViewById(R.id.llt_head);
        mBarLayout = findViewById(R.id.rlt_bar);
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        String keywords = getIntent().getStringExtra(key);

        List<MsgData> list = new ArrayList<>();
        //list.add(msgData);
        mAdapter = new NewsDetailAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
    }
}
