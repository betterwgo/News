package news.tencent.charco.android.mimc;

import android.os.Parcel;
import android.os.Parcelable;


public class MsgData extends NewsEsSource implements Parcelable {
    public Integer subType;

    public Integer total;

    public Integer curIndex;

    @Override
    public int describeContents() {
        return 0;
    }

    // 将对象的需要传递的属性 以 Parcel parcel.writXxx的形式写出，具体看属性的类型
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(nid);
        parcel.writeString(src);
        parcel.writeString(time);
        parcel.writeString(header);
        parcel.writeString(digest);
        parcel.writeString(content);
        parcel.writeString(href);
        parcel.writeString(releaseTime);
        parcel.writeString(news_type);
        parcel.writeLong(indexTime);
        parcel.writeInt(subType);
        parcel.writeInt(total);
        parcel.writeInt(curIndex);
    }

    public static final Parcelable.Creator<MsgData> CREATOR = new Creator<MsgData>() {

        @Override
        public MsgData createFromParcel(Parcel source) {
            MsgData msgData = new MsgData();
            msgData.nid = source.readString();
            msgData.src = source.readString();
            msgData.time = source.readString();
            msgData.header = source.readString();
            msgData.digest = source.readString();
            msgData.content = source.readString();
            msgData.href = source.readString();
            msgData.releaseTime = source.readString();
            msgData.news_type = source.readString();
            msgData.indexTime = source.readLong();
            msgData.subType = source.readInt();
            msgData.total = source.readInt();
            msgData.curIndex = source.readInt();
            return msgData;
        }

        @Override
        public MsgData[] newArray(int size) {
            return new MsgData[size];
        }
    };

    public Integer getSubType() {
        return subType;
    }

    public void setSubType(Integer subType) {
        this.subType = subType;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getCurIndex() {
        return curIndex;
    }

    public void setCurIndex(Integer curIndex) {
        this.curIndex = curIndex;
    }
}
