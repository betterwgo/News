package news.tencent.charco.android.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.Objects;

import news.tencent.charco.android.mimc.NewsEsSource;

public class NewsEs implements MultiItemEntity {
    /**
     * @return 0：东方财富
     */
    @Override
    public int getItemType(){
        return 0;
    }

    private NewsEsSource newsEsSource;

    public NewsEsSource getNewsEsSource() {
        return newsEsSource;
    }

    public void setNewsEsSource(NewsEsSource newsEsSource) {
        this.newsEsSource = newsEsSource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsEs newsEs = (NewsEs) o;
        return Objects.equals(newsEsSource.getNid(), newsEs.getNewsEsSource().getNid());
    }
}
