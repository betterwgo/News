package news.tencent.charco.android.mimc;

public class MimcConst {
    /**
     * 查询服务员
     */
    public static final String ADMIN_QUERY = "AdminQuery0911";
    /**
     * 详情服务员
     */
    public static final String ADMIN_DETAIL = "AdminDetail0911";
    /**
     * 关键字搜索服务员
     */
    public static final String ADMIN_KEYWORDS_SEARCH = "AdminKeywordsSearch0829";
}
