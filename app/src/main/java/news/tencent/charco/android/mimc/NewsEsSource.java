package news.tencent.charco.android.mimc;

import java.util.HashMap;
import java.util.Map;

public class NewsEsSource {
    public String nid;
    public String src;
    public String time;
    public String header;
    public String digest;
    public String content;
    public String href;
    public String releaseTime;
    public String news_type;
    public long indexTime;

    public Map<String, Object> toMap(){
        Map<String, Object> jsonMap = new HashMap<>();
        jsonMap.put("nid", nid);
        jsonMap.put("src", src);
        jsonMap.put("time", time);
        jsonMap.put("header", header);
        jsonMap.put("digest", digest);
        jsonMap.put("content", content);
        jsonMap.put("href",href);
        jsonMap.put("releaseTime",releaseTime);
        jsonMap.put("news_type",news_type);
        jsonMap.put("indexTime",indexTime);
        return jsonMap;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getNews_type() {
        return news_type;
    }

    public void setNews_type(String news_type) {
        this.news_type = news_type;
    }

    public long getIndexTime() {
        return indexTime;
    }

    public void setIndexTime(long indexTime) {
        this.indexTime = indexTime;
    }
}
